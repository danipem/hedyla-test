import React, { useState } from 'react'
import Mapa from './Mapa'


export const Calculator = () => {

    const initialState = {
        prices: '',
        finalValue: '',
        distance: '',
        origin: '',
        end: '',
        route: [],
        originMap: {
            lat: 0,
            lng: 0
        },
        dataOk: false,
        showForm: false
    }
    const [state, setState] = useState(initialState)

    const handleChange = (evt) => {
        const value = evt.target.value;
        setState({
            ...state,
            [evt.target.name]: value
        });
    }

    const handleSubmit = () => {
        setState(() => ({ ...state, finalValue: state.prices * state.distance }))
    }

    const handleAddPrice = (evt) => {
        setState(() => ({ ...state, prices: evt.target.value }))
    }

    const parseCoordinates = (rawCoordinates) => {
        let list = []
        for (let i = 0; i < rawCoordinates.length; i++) {
            list.push(
                {
                    lat: rawCoordinates[i][0],
                    lng: rawCoordinates[i][1]
                })
        }
        return list;
    }

    const parseOrigin = (rawCoordinates) => {
        return {
            lat: rawCoordinates[0][0],
            lng: rawCoordinates[0][1]
        }
    }

    const getDistance = () => {
        fetch(`http://router.project-osrm.org/route/v1/driving/${state.origin};${state.end}?geometries=geojson`)
            .then(response => response.json())
            .then(data => {
                if (data.code !== 'Ok') {
                    throw new Error(data.error);
                }
                return console.log(data) & setState(() => (
                    {
                        ...state,
                        distance: (data.routes[0].distance / 1000).toFixed(),
                        route: parseCoordinates(data.routes[0].geometry.coordinates),
                        originMap: parseOrigin(data.routes[0].geometry.coordinates),
                        dataOk: true
                    }
                ))
            })
            .catch(err => console.log(err));;
    }
    const showForm = () => {
        setState({ ...state, showForm: !state.showForm })
    }
    console.log(state)
    // 40.5985567,-3.7177708 tres cantos
    // 38.8793748,-7.0226987 badajoz
    return (
        <div>
            <h1 align="center">Insert your data</h1>
            <div className="forms">
                <div>
                    <div>
                        <p>Distance (in KM):</p>
                        <input type="number" value={state.distance} name="distance" onChange={handleChange} />
                    </div>
                    <div className="flex-row">
                        <div>
                            <p>Price (€/KM)</p>
                            <input type="number" value={state.prices} name="prices" onChange={handleChange} />
                        </div>
                        <div className="flex-column">
                            <button className="button_b" onClick={handleAddPrice} value="0.5">Truck (0.5 €/km)</button>
                            <button className="button_b" onClick={handleAddPrice} value="0.25">Van (0.25 €/km)</button>
                        </div>
                    </div>
                    <button className="button_d" onClick={handleSubmit}>Calculate final cost</button>
                    <p>You are getting the final price of {state.distance} km and {state.prices} €/km</p>
                    <p className="final-cost">Final cost: {state.finalValue} €</p>
                </div>

                <div className={state.showForm ? "display-block" : 'display-none'}>
                    <div>
                        <p>Origin:</p>
                        <input type="text" value={state.origin} name="origin" onChange={handleChange} />
                    </div>

                    <div>
                        <p>End:</p>
                        <input type="text" value={state.end} name="end" onChange={handleChange} />
                    </div>
                    <p className="warning">Please, enter the coordinates in this format (latitude,longitude with no spaces): 38.8793748,-7.0226987</p>
                    <button className="button_d" onClick={getDistance}>Calculate distance</button>
                    <p>The distance: {state.distance} km</p>
                    <button onClick={showForm} className={state.showForm ? "button_c" : 'display-none'}>Hide coordinates</button>

                </div>
                <button onClick={showForm} className={state.showForm ? "display-none" : 'button_c'}>Insert coordinates</button>
            </div>
            <Mapa
                route={state.route}
                origin={state.originMap}
            />
        </div>


    )
}
