import React, { Component } from 'react'
import { Map, GoogleApiWrapper, Polyline } from 'google-maps-react';

export class Mapa extends Component {

  render() {
      const mapStyles = {          
          width: '100%',
          height: '60%'
      }

    return (
      <Map
        google={this.props.google}
        zoom={6}
        style={mapStyles}
        initialCenter={this.props.origin}
        center={this.props.origin}
      >
        <Polyline
          path={this.props.route}
          strokeColor="#0000FF"
          strokeOpacity={0.8}
          strokeWeight={2} />
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyDz6Aavt1fPzKVRZCqNXURamMUgSEW_-hA'
})(Mapa);